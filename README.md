### This repository explains how install and configure Linux on Odroid XU4.###

### Linux Instalation ###

* These instructions were adapted from [here](http://barzinm.com/robotics/2016/odriod-setup.html).

* To install linux in the SDCARD you will going to need a a host linux computer with SDCARD slot. Insert the SDCARD and find out its device name:


```
#!bash

sudo fdisk -l
```
It should be `/dev/sda` or `/dev/sdb`. Run the command before and after inserting the card to make sure.

* Download linux:

```
#!bash

wget http://odroid.in/ubuntu_14.04lts/ubuntu-14.04.1lts-lubuntu-odroid-xu3-20150212.img.xz
```

* Assuming the SDCARD is on /dev/sdb, discompact and flash linux into the SD:
```
#!bash

unxz ubuntu-14.04lts-server-odroid-xu3-20150725.img.xz # or use tar
sudo dd if=/dev/zero of=/dev/sdb bs=1M # change sdb if necessary
sync
sudo fdisk -l # [optional] it should say '/dev/sd?' doesn't contain a valid partition
sudo dd if=ubuntu-14.04lts-server-odroid-xu3-20150725.img of=/dev/sdb bs=1M conv=fsync # change sdb if necessary
sync
```

* Eject the SD card and put it in the Odroid and power it up.

* When you first install the mentioned Ubuntu image, the user name and password for the image are respectively `root` and `odroid`.

### Linux Upgrade ###

To upgrade the linux Kernel to Version 4.2, follow the instructions given [here](https://github.com/umiddelb/armhf/wiki/How-To-compile-a-custom-Linux-kernel-for-your-ARM-device) and reproduced as:


```
#!bash

git clone --depth 1 --single-branch -b odroidxu4-v4.2 https://github.com/tobetter/linux
cd linux 
make odroidxu4_defconfig
make -j 8 zImage dtbs modules
sudo cp arch/arm/boot/zImage arch/arm/boot/dts/*.dtb /media/boot
sudo make modules_install
sudo make firmware_install
sudo make headers_install INSTALL_HDR_PATH=/usr
kver=`make kernelrelease`
sudo cp .config /boot/config-${kver}
cd /boot
sudo update-initramfs -c -k ${kver}
sudo mkimage -A arm -O linux -T ramdisk -a 0x0 -e 0x0 -n initrd.img-${kver} -d initrd.img-${kver} uInitrd-${kver}
sudo cp uInitrd-${kver} /media/boot/uInitrd
```

### WiFi instalation ###

A instalação do adaptador WiFi **RALINK WIRELESS DEVICE MEDIATEK MT7601** pressupõe que o Kernel está na versão 4.2, como acima. Versões anteriores não possuem driver para o disposivo.

* Verifique se o seu dispositivo está funcionando:


```
#!bash

lsusb
```
Você deve ver algo assim:
 

```
#!bash

Bus 001 Device 005: ID 148f:7601 Ralink Technology, Corp.
```

Caso não veja, troque a porta e tente novamente. Caso ainda não veja, o problema é mais grave. O dispositivo pode estar queimado.

Copie o firmware do disposivo para o diretório correto e reinicialize o computador:


```
#!bash

sudo wget https://bitbucket.org/coroufmg/odroid/src/9fbad87976b510167b7b46ef806d6a01fe9a79a3/mt7601u.bin -O /lib/firmware/mt7601u.bin
 
reboot
```
* Check if the module of the device was loaded:
 

```
#!bash

lsmod
 
```
You should see something like:
 
```
#!bash


mt7601u 73787 0
 
```
If you cannot see the module, you need to load it manually:

```
#!bash

sudo modprobe -r mt7601u && sudo modprobe mt7601u
```
Check if the module was loaded. if it was loaded, insert the line `mt7601u` to the file `/etc/modules.conf`. To edit the file run:


```
#!bash

sudo nano /etc/modules.conf
```

* Restart the computer and test:


```
#!bash

ifconfig
```
You should be able to see a device called `wlanXX` (e.g. wlan0, wlan2 or wlan3).