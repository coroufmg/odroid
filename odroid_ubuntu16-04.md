# Guia de instalação e configuração do Ubuntu 16.04 + DJI\_SDK no Odroid XU4

## 1) Instalação do Ubuntu 16.04

- Baixar a imagem do sistema. A versão recomendada é a *ubuntu-16.04.2-mate-odroid-xu4-20170510.img.xz*
Disponĩvel em: [CORO](http://coro.cpdee.ufmg.br/files/odroid/ubuntu-16.04.2-mate-odroid-xu4-20170510.img.xz) ou [odroid.in](https://odroid.in/ubuntu_16.04lts/ubuntu-16.04.2-mate-odroid-xu4-20170510.img.xz)

- Instalar o [Etcher](http://coro.cpdee.ufmg.br/files/odroid/etcher-1.1.2-linux-x86_64.zip) e utilizar para copiar a imagem do Ubuntu no SD Card do Odroid.

- Utilizar o gparted para expandir a tamanho da unidade memária para o máximo disponível do SD Card.
```
#!bash
sudo gparted
```
- Inserir o cartão SD no Odroid e ligar.
- Login: odroid senha: odroid

## 2) Instalação do sistema DJI SDK
- Instalar o [ROS Kinetic](http://wiki.ros.org/kinetic/Installation/Ubuntu)
- Instalar o [DJI SDK](https://developer.dji.com/onboard-sdk/documentation/sample-doc/sample-setup.html) ou [wiki.ros.org](http://wiki.ros.org/dji_sdk/Tutorials/Getting%20Started)
- Configurar o wi-fi.
- Configurar o auto-login:

Nas configurações de usuário colocar para fazer login sem pedir senha no Odroid.

Criar o seguinte arquivo:
```
#!bash
sudo nano /etc/lightdm/lightdm.conf.d/60-lightdm-gtk-greeter.conf
```
E adicionar as configurações de auto-login:
```
[SeatDefaults]
greeter-session=lightdm-gtk-greeter
autologin-user=odroid
```
- Configurar launch atutomatico ao ligar o odroid:
    * Em Applications Startup, criar um novo item com o seguinte comando:
```
#!bash
bash /home/odroid/dji_ws/auto_init.sh
```
Criar o arquivo *auto\_init.sh* com os seguintes comandos:
```
#!bash
#!/bin/bash
cd /home/odroid/dji_ws/
source devel/setup.bash
roslaunch path_follow path_follow_dji.launch
#roslaunch dji_m100 first_fly.launch
#roslaunch field_follow follow.launch
```
Ao ligar o Odroid os camando serão executados automaticamente.

